import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CestquoiPage } from './cestquoi.page';

describe('CestquoiPage', () => {
  let component: CestquoiPage;
  let fixture: ComponentFixture<CestquoiPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CestquoiPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CestquoiPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
