import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: 'tabs4', loadChildren: './tabs4/tabs4.module#Tabs4PageModule' },
  { path: 'welecome', loadChildren: './welecome/welecome.module#WelecomePageModule' },
  { path: 'mesure', loadChildren: './mesure/mesure.module#MesurePageModule' },
  { path: 'cestquoi', loadChildren: './cestquoi/cestquoi.module#CestquoiPageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
