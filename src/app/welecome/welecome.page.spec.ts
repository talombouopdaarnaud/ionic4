import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WelecomePage } from './welecome.page';

describe('WelecomePage', () => {
  let component: WelecomePage;
  let fixture: ComponentFixture<WelecomePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WelecomePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WelecomePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
