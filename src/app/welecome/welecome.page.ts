import { Component, OnInit } from '@angular/core';
import { IonSlides } from '@ionic/angular';

@Component({
  selector: 'app-welecome',
  templateUrl: './welecome.page.html',
  styleUrls: ['./welecome.page.scss'],
})
export class WelecomePage implements OnInit {
  slideOptions = {
    initialSlide: 1,
    speed: 400,
  };
  constructor() { }
  slidesDidLoad(slides: IonSlides) {
    slides.startAutoplay();
  }

  ngOnInit() {
  }

}
