import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MesurePage } from './mesure.page';

describe('MesurePage', () => {
  let component: MesurePage;
  let fixture: ComponentFixture<MesurePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MesurePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MesurePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
